from crm import models
from django import forms
from django.core.exceptions import ValidationError
from multiselectfield.forms.fields import MultiSelectFormField
import hashlib


class RegForm(forms.ModelForm):
    re_password = forms.CharField(min_length=6, widget=forms.PasswordInput(attrs={'placeholder': '确认密码'}), label='确认密码')
    password = forms.CharField(min_length=6, widget=forms.PasswordInput(attrs={'placeholder': '输入密码'}), label='密码')

    def clean(self):
        self._validate_unique = True  # 校验字段的唯一性
        password = self.cleaned_data.get('password')
        re_password = self.cleaned_data.get('re_password')
        if password == re_password:
            md5 = hashlib.md5()
            md5.update(password.encode('utf-8'))

            self.cleaned_data['password'] = md5.hexdigest()
            return self.cleaned_data
        self.add_error('re_password', '两次密码不一致!!')
        raise ValidationError('两次密码不一致')

    class Meta:
        model = models.UserProfile
        fields = '__all__'  # ['username', 'password']
        exclude = ['memo', 'is_active']
        widgets = {
            'username': forms.TextInput(attrs={'placeholder': '请输入邮箱'}),
            # 'password': forms.PasswordInput(attrs={'placeholder': '请输入密码'}),
            'mobile': forms.TextInput(attrs={'placeholder': '请输入手机号'}),
            'name': forms.TextInput(attrs={'placeholder': '请输入姓名'}),
        }

        error_messages = {
            'username': {}
        }
        # labels = {
        #     'username':'xxx'
        # }
        #


class BSForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # 自定义的操作
        for name, field in self.fields.items():
            # print(name, field)
            if isinstance(field, (forms.BooleanField, MultiSelectFormField)):
                continue
            field.widget.attrs['class'] = 'form-control'


class CustomerForm(BSForm):
    class Meta:
        model = models.Customer
        fields = '__all__'
        # widgets = {
        #     'qq': forms.TextInput(attrs={'class': 'form-control'})
        # }


class ConsultRecordForm(BSForm):
    class Meta:
        model = models.ConsultRecord
        fields = '__all__'

    def __init__(self, request, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # print(list(self.fields['customer'].choices))
        # 客户 ——  当前登录用户的客户
        self.fields['customer'].choices = [('', '---------')] + [(i.pk, str(i)) for i in
                                                                 request.user_obj.customers.all()]

        self.fields['consultant'].choices = [(request.user_obj.pk, str(request.user_obj))]


class EnrollmentForm(BSForm):
    class Meta:
        model = models.Enrollment
        fields = '__all__'

        # labels = {'school':'xxx'}

    # def __init__(self, customer_id, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #
    #     self.fields['customer'].choices = [(customer_id, models.Customer.objects.get(pk=customer_id))]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # self.instance   传进来的对象

        self.fields['customer'].choices = [(self.instance.customer_id, self.instance.customer)]
        self.fields['enrolment_class'].choices = [(i.pk, str(i)) for i in self.instance.customer.class_list.all()]


class ClassListForm(BSForm):
    class Meta:
        model = models.ClassList
        fields = '__all__'

        widgets = {
            'start_date': forms.TextInput(attrs={'autocomplete': 'off', 'type': 'date'})
        }


class CourseRecordForm(BSForm):
    class Meta:
        model = models.CourseRecord
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['re_class'].choices = [(self.instance.re_class_id, self.instance.re_class)]
        self.fields['recorder'].choices = [(self.instance.recorder_id, self.instance.recorder)]


class StudyRecordForm(BSForm):
    class Meta:
        model = models.StudyRecord
        fields = '__all__'
