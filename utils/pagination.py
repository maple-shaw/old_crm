from django.utils.safestring import mark_safe
from django.http.request import QueryDict


class Pagination:

    def __init__(self, page, data_count, qd=None, per_num=10, max_show=11):

        try:
            page = int(page)
            if page <= 0:
                page = 1

        except  Exception:
            page = 1
        if not qd:
            qd = QueryDict(mutable=True)

        self.qd = qd

        self.page = page
        per_num = per_num  # 每页显示的数据条数

        """
        1    0    10
        2    10   20
        """

        page_count, more = divmod(data_count, per_num)  # 总的页码数
        if more:
            page_count += 1
        self.page_count = page_count

        # 显示的页码数
        max_show = max_show
        half_show = max_show // 2

        self.start = (page - 1) * per_num  # 切片的起始值
        self.end = page * per_num  # 切片的终止值

        if page_count < max_show:
            # 页码的总数小于要显示的页码数
            page_start = 1
            page_end = page_count
        else:
            if page - half_show <= 0:
                page_start = 1
                page_end = max_show

            elif page + half_show > page_count:
                page_start = page_count - max_show + 1
                page_end = page_count
            else:
                page_start = page - half_show
                page_end = page + half_show

        self.page_start = page_start
        self.page_end = page_end

    @property
    def page_html(self):

        page_list = []

        self.qd['page'] = 1
        page_list.append(
            '<li ><a href="?{}" aria-label="Previous">首页</a></li>'.format(self.qd.urlencode()))

        if self.page == 1:
            page_list.append(
                '<li class="disabled"><a  aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>')
        else:
            self.qd['page'] = self.page - 1
            page_list.append(
                '<li><a href="?{}" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>'.format(
                    self.qd.urlencode()))

        for num in range(self.page_start, self.page_end + 1):
            self.qd['page'] = num
            if self.page == num:
                page_list.append('<li class="active"><a >{}</a></li>'.format(num))
            else:
                page_list.append('<li><a href="?{}">{}</a></li>'.format(self.qd.urlencode(), num))

        if self.page == self.page_count:
            page_list.append(
                '<li class="disabled"><a aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>')
        else:

            self.qd['page'] = self.page + 1

            page_list.append(
                '<li><a href="?{}" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>'.format(
                    self.qd.urlencode()))
        self.qd['page'] = self.page_count
        page_list.append(
            '<li ><a href="?{}" aria-label="Previous">尾页</a></li>'.format(self.qd.urlencode()))

        return mark_safe(
            '<nav aria-label="Page navigation"><ul class="pagination">{}</ul></nav>'.format(''.join(page_list)))
