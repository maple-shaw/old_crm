from django.conf.urls import url
from crm.views import consultant, teacher

urlpatterns = [
    url(r'^login/$', consultant.Login.as_view(), name='login'),
    url(r'^reg/$', consultant.reg, name='reg'),
    # 公户
    url(r'^customers/$', consultant.Customer.as_view(), name='customers'),
    # 私户
    url(r'^my_customers/$', consultant.Customer.as_view(), name='my_customers'),

    url(r'^add_customer/$', consultant.customer_change, name='add_customer'),
    url(r'^edit_customer/(\d+)/$', consultant.customer_change, name='edit_customer'),
    # 展示是某个销售的所有的跟进记录
    url(r'^consult_record/$', consultant.ConsultRecord.as_view(), name='consult_record'),

    # 展示是某个客户的所有的跟进记录
    url(r'^consult_record/(\d+)/$', consultant.ConsultRecord.as_view(), name='one_consult_record'),

    url(r'^add_consult_record/$', consultant.consult_record_change, name='add_consult_record'),
    url(r'^edit_consult_record/(\d+)/$', consultant.consult_record_change, name='edit_consult_record'),

    url(r'^enrollment_record/(?P<customer_id>\d+)/$', consultant.EnrollmentRecord.as_view(), name='enrollment_record'),
    url(r'^add_enrollment_record/(?P<customer_id>\d+)/$', consultant.enrollment_record_change,
        name='add_enrollment_record'),
    url(r'^edit_enrollment_record/(?P<pk>\d+)/$', consultant.enrollment_record_change, name='edit_enrollment_record'),

    url(r'^class_list/$', teacher.Classes.as_view(), name='class_list'),

    url(r'^add_class/$', teacher.class_change, name='add_class'),
    url(r'^edit_class/(\d+)/$', teacher.class_change, name='edit_class'),

    url(r'^course_record/(?P<class_id>\d+)/$', teacher.CourseRecordList.as_view(), name='course_record'),

    url(r'^add_course_record/(?P<class_id>\d+)/$', teacher.course_record_change, name='add_course_record'),
    url(r'^edit_course_record/(?P<pk>\d+)/$', teacher.course_record_change, name='edit_course_record'),

    url(r'^study_record/(?P<course_record_id>\d+)/$', teacher.study_record, name='study_record'),

]
