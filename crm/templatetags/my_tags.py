from django import template
from django.urls import reverse

register = template.Library()


@register.simple_tag
def reverse_url(request, name, *args, **kwargs):
    next = request.get_full_path()
    from django.http.request import QueryDict
    qd = QueryDict(mutable=True)
    qd['next'] = next

    return "{}?{}".format(reverse(name, args=args, kwargs=kwargs), qd.urlencode())
