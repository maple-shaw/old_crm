from django.shortcuts import render, HttpResponse, redirect, reverse
from django.views import View
from crm import models
from utils.pagination import Pagination
import hashlib
from crm.forms import ClassListForm, CourseRecordForm, StudyRecordForm
from django.db.models import Q
from .base import ShowList


class Classes(ShowList):

    def get(self, request, *args, **kwargs):
        q = self.search([])

        all_class_list = models.ClassList.objects.filter(q)

        return render(request, 'teacher/class.html', {'all_class_list': all_class_list})


def class_change(request, pk=None):
    obj = models.ClassList.objects.filter(pk=pk).first()
    form_obj = ClassListForm(instance=obj)
    if request.method == 'POST':
        form_obj = ClassListForm(request.POST, instance=obj)
        if form_obj.is_valid():
            form_obj.save()
            next = request.GET.get('next')
            if next:
                return redirect(next)
            return redirect('class_list')
    title = '编辑班级' if pk else '新增班级'
    return render(request, 'form.html', {'form_obj': form_obj, 'title': title})


class CourseRecordList(ShowList):

    def get(self, request, class_id, *args, **kwargs):
        q = self.search([])

        all_course_records = models.CourseRecord.objects.filter(q, re_class_id=class_id)

        return render(request, 'teacher/course_record.html',
                      {'all_course_records': all_course_records, 'class_id': class_id})

    def multi_init(self):
        # 批量初始化学习记录

        pks = self.request.POST.get('pk')

        for pk in pks:
            # 一个课程记录
            course_record_obj = models.CourseRecord.objects.get(pk=pk)
            students = course_record_obj.re_class.customer_set.filter(status='studying')

            study_record_obj_list = []

            for student in students:

                if not models.StudyRecord.objects.filter(course_record=course_record_obj, student=student).exists():
                    study_record_obj_list.append(models.StudyRecord(course_record=course_record_obj, student=student))

            if study_record_obj_list:
                models.StudyRecord.objects.bulk_create(study_record_obj_list)


def course_record_change(request, class_id=None, pk=None):
    obj = models.CourseRecord(re_class_id=class_id,
                              recorder=request.user_obj) if class_id else models.CourseRecord.objects.filter(
        pk=pk).first()

    form_obj = CourseRecordForm(instance=obj)

    if request.method == 'POST':
        form_obj = CourseRecordForm(request.POST, instance=obj)
        if form_obj.is_valid():
            form_obj.save()
            print(request.GET)
            next = request.GET.get('next')
            if next:
                return redirect(next)
            return redirect(reverse('course_record', args=(class_id,)))

    title = '新增课程记录'
    return render(request, 'form.html', {'form_obj': form_obj, 'title': title})


from django.forms import modelformset_factory


def study_record(request, course_record_id):
    ModelFormSet = modelformset_factory(models.StudyRecord, StudyRecordForm, extra=0)
    study_records = models.StudyRecord.objects.filter(course_record_id=course_record_id)
    formset = ModelFormSet(queryset=study_records)

    if request.method == 'POST':
        formset = ModelFormSet(request.POST)
        if formset.is_valid():
            formset.save()
        print(formset.errors)
    return render(request, 'teacher/study_record.html', {'formset': formset})
