from django.shortcuts import render, HttpResponse, redirect, reverse
from django.views import View
from django.db.models import Q


class ShowList(View):
    def post(self, request, *args, **kwargs):
        action = request.POST.get('action')

        func = getattr(self, action, None)
        if not func:
            # 方法未定义
            return HttpResponse('非法操作')
        # 执行相应的方法
        ret = func()
        if ret:
            return ret

        return self.get(request, *args, **kwargs)

    def search(self, filed_name):
        query = self.request.GET.get('query', '')

        q = Q()
        q.connector = 'OR'
        for field in filed_name:
            q.children.append(Q(('{}__contains'.format(field), query)))

        return q
