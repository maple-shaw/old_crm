from django.utils.deprecation import MiddlewareMixin
from django.shortcuts import redirect, reverse
from crm import models


class AuthMiddleWare(MiddlewareMixin):

    def process_request(self, request):
        is_login = request.session.get('is_login')

        if request.path_info in [reverse('login'), reverse('reg')] or request.path_info.startswith('/admin'):
            return

        if not is_login:
            # 没有登录
            return redirect('login')
        # 每次请求获取到当前登录的对象
        request.user_obj = models.UserProfile.objects.get(pk=request.session.get('pk'))
