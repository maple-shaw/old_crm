from django.shortcuts import render, HttpResponse, redirect, reverse
from django.views import View
from crm import models
from utils.pagination import Pagination
import hashlib
from crm.forms import RegForm, CustomerForm, ConsultRecordForm, EnrollmentForm
from django.db.models import Q
from .base import ShowList


# Create your views here.
class Login(View):

    def get(self, request, *args, **kwargs):
        return render(request, 'login.html')

    def post(self, request, *args, **kwargs):
        username = request.POST.get('username')
        password = request.POST.get('password')
        md5 = hashlib.md5()
        md5.update(password.encode('utf-8'))
        obj = models.UserProfile.objects.filter(username=username, password=md5.hexdigest(), is_active=True).first()
        if obj:
            # 登录成功
            # 保存用户的信息
            request.session['pk'] = obj.pk
            request.session['is_login'] = True
            return redirect('my_customers')

        return render(request, 'login.html', {'error': '用户名或密码错误'})


def reg(request):
    form_obj = RegForm()
    if request.method == 'POST':
        form_obj = RegForm(request.POST)
        if form_obj.is_valid():  # 校验数据
            # print(form_obj.cleaned_data)
            form_obj.save()
            return redirect('login')

    return render(request, 'reg.html', {'form_obj': form_obj})


def customers(request):
    if request.path_info == reverse('customers'):
        all_customers = models.Customer.objects.filter(consultant=None)  # 公户
    else:
        # 私户
        # all_customers = models.Customer.objects.filter(consultant_id=request.session.get('pk'))
        all_customers = models.Customer.objects.filter(consultant=request.user_obj)

    return render(request, 'consultant/customers.html',
                  {'all_customers': all_customers})


class Customer(ShowList):

    def get(self, request, *args, **kwargs):
        url = reverse('customers')
        if request.path_info == url:
            all_customers = models.Customer.objects.filter(consultant=None)  # 公户
        else:
            # 私户
            # all_customers = models.Customer.objects.filter(consultant_id=request.session.get('pk'))
            all_customers = models.Customer.objects.filter(consultant=request.user_obj)

        q = self.search(['qq', 'name', 'class_list__semester'])
        all_customers = all_customers.filter(q, ).distinct()

        page = Pagination(request.GET.get('page'), all_customers.count(), request.GET.copy(), per_num=2)

        return render(request, 'consultant/customers.html',
                      {'all_customers': all_customers[page.start:page.end],
                       'page_html': page.page_html,
                       'url': url,
                       })

    def multi_apply(self):
        # 公户转私户
        pks = self.request.POST.getlist('pk')
        # 修改客户的销售为当前的用户
        # from old_crm import settings
        from django.conf import global_settings, settings

        if self.request.user_obj.customers.count() + len(pks) > settings.MAX_CUSTOMER_NUM:
            return HttpResponse('做人留一线，日后好相见')

        from django.db import transaction

        with transaction.atomic():
            # 事务
            query_set = models.Customer.objects.filter(pk__in=pks, consultant=None).select_for_update()  # 加锁

            if len(pks) == query_set.count():

                query_set.update(consultant=self.request.user_obj)
            else:
                return HttpResponse('你的手速太慢了，需要再练练')

        # self.request.user_obj.customers.add(*models.Customer.objects.filter(pk__in=pks))

    def multi_pub(self):
        # 私户转公户

        pks = self.request.POST.getlist('pk')
        # 修改客户的销售为空
        # models.Customer.objects.filter(pk__in=pks).update(consultant=None)

        self.request.user_obj.customers.remove(*models.Customer.objects.filter(pk__in=pks))

    # def multi_delete(self):
    #     # 私户转公户
    #     pass


users = [{'name': 'alex-{}'.format(i), 'pwd': 'dsb'} for i in range(1, 355)]


def user_list(request):
    page_obj = Pagination(request.GET.get('page'), len(users))

    return render(request, 'user_list.html',
                  {'users': users[page_obj.start:page_obj.end], 'page_html': page_obj.page_html})


def add_customer(request):
    form_obj = CustomerForm()  # 不包含任何数据
    if request.method == 'POST':
        form_obj = CustomerForm(request.POST)
        if form_obj.is_valid():
            form_obj.save()
            return redirect('customers')

    return render(request, 'consultant/add_customer.html', {'form_obj': form_obj})


def edit_customer(request, pk):
    obj = models.Customer.objects.filter(pk=pk).first()
    form_obj = CustomerForm(instance=obj)  # 包含了原始数据
    if request.method == 'POST':
        form_obj = CustomerForm(request.POST, instance=obj)
        if form_obj.is_valid():
            form_obj.save()  # 编辑了原始的数据
            return redirect('customers')

    return render(request, 'consultant/edit_customer.html', {'form_obj': form_obj})


def customer_change(request, pk=None):
    obj = models.Customer.objects.filter(pk=pk).first()
    form_obj = CustomerForm(instance=obj)  # 包含了原始数据
    if request.method == 'POST':
        form_obj = CustomerForm(request.POST, instance=obj)
        if form_obj.is_valid():
            form_obj.save()
            next = request.GET.get('next')
            return redirect(next)
    title = '编辑客户' if pk else '新增客户'
    return render(request, 'form.html', {'form_obj': form_obj, 'title': title})


class ConsultRecord(ShowList):
    def get(self, request, customer_id=None, *args, **kwargs):
        q = self.search(['customer__name', 'customer__qq', 'note', 'date'])

        if customer_id:
            all_consult_records = models.ConsultRecord.objects.filter(q, customer_id=customer_id,
                                                                      delete_status=False).order_by('-date')
        else:
            all_consult_records = models.ConsultRecord.objects.filter(q, consultant=request.user_obj,
                                                                      delete_status=False).order_by('-date')

        page = Pagination(request.GET.get('page'), all_consult_records.count(), request.GET.copy(), per_num=2)

        return render(request, 'consultant/consult_record.html',
                      {'all_consult_records': all_consult_records[page.start:page.end],
                       'page_html': page.page_html,

                       })


def consult_record_change(request, pk=None):
    obj = models.ConsultRecord.objects.filter(pk=pk).first()
    form_obj = ConsultRecordForm(request, instance=obj)  # 包含了原始数据
    if request.method == 'POST':
        form_obj = ConsultRecordForm(request, data=request.POST, instance=obj)
        if form_obj.is_valid():
            form_obj.save()
            next = request.GET.get('next')
            if next:
                return redirect(next)
            return redirect('consult_record')
    title = '编辑跟进记录' if pk else '新增跟进记录'
    return render(request, 'form.html', {'form_obj': form_obj, 'title': title})


class EnrollmentRecord(ShowList):

    def get(self, request, customer_id, *args, **kwargs):
        all_enrollment_records = models.Enrollment.objects.filter(customer_id=customer_id)

        return render(request, 'consultant/enrollment_record.html', {'all_enrollment_records': all_enrollment_records})


def enrollment_record_change(request, customer_id=None, pk=None):
    obj = models.Enrollment.objects.filter(pk=pk).first()
    if not customer_id:
        customer_id = obj.customer_id
    form_obj = EnrollmentForm(customer_id, instance=obj)
    if request.method == 'POST':
        form_obj = EnrollmentForm(customer_id, data=request.POST, instance=obj)
        if form_obj.is_valid():
            form_obj.save()
            return redirect('my_customers')

    title = '新增报名记录' if not pk else '编辑报名记录'
    return render(request, 'form.html', {'form_obj': form_obj, 'title': title})


def enrollment_record_change(request, customer_id=None, pk=None):
    obj = models.Enrollment(customer_id=customer_id) if not pk else models.Enrollment.objects.filter(pk=pk).first()

    form_obj = EnrollmentForm(instance=obj)
    if request.method == 'POST':
        form_obj = EnrollmentForm(data=request.POST, instance=obj)
        if form_obj.is_valid():
            form_obj.save()
            next = request.GET.get('next')
            if next:
                return redirect(next)
            return redirect('my_customers')

    title = '新增报名记录' if not pk else '编辑报名记录'
    return render(request, 'form.html', {'form_obj': form_obj, 'title': title})
